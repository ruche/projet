#include <Arduino.h>
#include <WiFiEsp.h>
#include <WiFiEspClient.h>
#include <PubSubClient.h>
#include <QTRSensors.h>
#include <HX711.h>
#include <SHT1x.h>
#include <OneWire.h> // Temperature

char MQTT_BUF1[16]; // 2 Buffers utilisés un peu partout
char MQTT_BUF2[16];

void envoie_mqtt(const char *topic, long val); // Fonction pour envoyer une valeur sur un canal

/* ----- *
 * MASSE *
 * ----- */

#define calibration_factor1 -126870
#define calibration_factor2 -128470
#define calibration_factor3 -128790
#define calibration_factor4 -134610
#define T .1f

HX711 scale1(7, 6);  //BD
HX711 scale2(9, 6);  //BG
HX711 scale3(10, 6); //HG
HX711 scale4(8, 6);  //HD

float Somme, Load1, Load2, Load3, Load4;

void init_masse()
{
    Serial.println("-- Initialisation: Masse");

    scale1.set_scale(calibration_factor1);
    scale1.tare();

    scale2.set_scale(calibration_factor2);
    scale2.tare();

    scale3.set_scale(calibration_factor3);
    scale3.tare();

    scale4.set_scale(calibration_factor4);
    scale4.tare();

#define Scale1 scale1.get_units()
#define Scale2 scale2.get_units()
#define Scale3 scale3.get_units()
#define Scale4 scale4.get_units()

    Load1 = Scale1;
    Load2 = Scale2;
    Load3 = Scale3;
    Load4 = Scale4;
}

void mesure_masse()
{

    Serial.println("-- Mesure: Masse");

    for (int i = 1; i <= 8;)
    {
#define Scale1 scale1.get_units()
#define Scale2 scale2.get_units()
#define Scale3 scale3.get_units()
#define Scale4 scale4.get_units()

        if (!(Scale1 > abs(Load1 + 2) * 4))
        {
            Load1 = Load1 + Scale1;
        }

        if (!(Scale2 > abs(Load2 + 2) * 4))
        {
            Load2 = Load2 + Scale2;
        }

        if (!(Scale3 > abs(Load3 + 2) * 4))
        {
            Load3 = Load3 + Scale3;
        }

        if (!(Scale4 > abs(Load4 + 2) * 4))
        {
            Load4 = Load4 + Scale4;
            i++;
        }
    }

    Load1 = Load1 / 8;
    Load2 = Load2 / 8;
    Load3 = Load3 / 8;
    Load4 = Load4 / 8;
    Somme = Load1 + Load2 + Load3 + Load4;
    float x = (Load1 + Load4) / (Load1 + Load2 + Load3 + Load4);
    float y = (Load3 + Load4) / (Load1 + Load2 + Load3 + Load4);

    Serial.println();
    Serial.print("Reading:  ");
    Serial.print("BD ");
    Serial.print(Load1);
    Serial.print("  ");
    Serial.print("BG ");
    Serial.print(Load2);
    Serial.print("  ");
    Serial.print("HG ");
    Serial.print(Load3);
    Serial.print("  ");
    Serial.print("HD ");
    Serial.print(Load4);
    Serial.print("  ");
    Serial.print("Somme: ");
    Serial.print(Somme, 3);

    if (Serial.available())
    {
        char reset = Serial.read();
        if (reset == '0')
            scale1.tare();
        scale2.tare();
        scale3.tare();
        scale4.tare();
        Serial.println();
        Serial.println("RESET");
    }
}

void envoie_masse()
{
    /*
    envoie_mqtt("masse BD", Scale1);
    envoie_mqtt("masse BG", Scale2);
    envoie_mqtt("masse HD", Scale3);
    envoie_mqtt("masse HG", Scale4);
    envoie_mqtt("Masse Somme", Somme);
    envoie_mqtt("Position x", x);
    envoie_mqtt("Position y", y); */
}

/* ------------- *
 * FREQUENTATION *
 * ------------- */

const int FREQ_NCARTES = 1;                               // Nombre de modules de 8 capteurs
const int FREQ_PAR_CARTE = 8;                             // Nombre de capteurs par module
const int FREQ_NCAPTEURS = FREQ_NCARTES * FREQ_PAR_CARTE; // Nombre total de capteurs
const int FREQ_TIMEOUT = 3000;                            // 'Timeout' maximal (us)

const int FREQ_EMITTERPIN = 4; // Pin pour l'alimentation des émtteurs
const uint8_t FREQ_PINS[] =    // Pins des capteurs
    {2, 3, 16, 17, 18, 19, 20, 21,
     22, 23, 24, 25, 26, 27, 28, 29,
     30, 31, 32, 33, 34, 35, 36, 37,
     38, 39, 40, 41, 42, 43, 44, 45,
     46, 47, 48, 49, 50, 51, 52, 53};

QTRSensors qtr; // Objet de la bibliotheque

long freq_nmesures;             // Nombre de mesures effectuées
long freq_zone[FREQ_NCAPTEURS]; // Nombre de détections pour chaque capteur

uint16_t freq_buf[FREQ_NCAPTEURS]; // Buffer de lecture

void init_freq()
{
    Serial.println("-- Initialisation: Frequentation");

    qtr.setTypeRC(); // Paramètrage de la bibliotheque
    qtr.setTimeout(FREQ_TIMEOUT);
    qtr.setEmitterPin(FREQ_EMITTERPIN);
    qtr.setSensorPins(FREQ_PINS, FREQ_NCAPTEURS);
}

void mesure_freq()
{
    Serial.println("-- Mesure: Frequentation");

    qtr.read(freq_buf); // Lecture

    Serial.print("Freq: ");
    for (int i = 0; i < FREQ_NCAPTEURS; i++) // Pour chaque capteur
    {
        Serial.print('\t');
        Serial.print(freq_buf[i]);

        if (freq_buf[i] <= FREQ_TIMEOUT / 2) // Si on detecte une abeille
            freq_zone[i]++;                  // On incremente le nombre de detection de ce capteur
    }
    Serial.print('\n');

    freq_nmesures++;
}

void envoie_freq()
{
    long freq_total = 0; // On va compter le nombre total de detections

    for (int i = 0; i < FREQ_NCAPTEURS; i++) // Pour chaque capteur
    {
        freq_total += freq_zone[i]; // On ajoute les détections au calcul du total

        strcpy(MQTT_BUF2, "freq/zone_");      // On construit une chaîne de la forme
        itoa(i, MQTT_BUF1, 10);               //   "freq/zone_X" avec X le numéro du capteur
        strcat(MQTT_BUF2, MQTT_BUF1);         //   puis on envoie sur le canal "freq/zone_X"
        envoie_mqtt(MQTT_BUF2, freq_zone[i]); //   le nombre de détections du capteur

        strcpy(MQTT_BUF2, "freq/pourcent_");          // Même chose mais avec le pourcentage
        itoa(i, MQTT_BUF1, 10);                       //   de détections effectuées par ce capteur
        strcat(MQTT_BUF2, MQTT_BUF1);                 //   par rapport au total
        int p = (freq_zone[i] * 100) / freq_nmesures; // Calcul du pourcentage
        envoie_mqtt(MQTT_BUF2, p);                    // On envoie
    }

    envoie_mqtt("bees/freq/total", freq_total); // On envoie le nombre total de détections
    memset(freq_zone, 0, sizeof(freq_zone));    // On remet a zéro les détections
    freq_nmesures = 0;                          //   et le nombre de détections totales
}

/* ----------- *
 * HYGROMETRIE *
 * ----------- */

int hygro_int; // *10 (72,5% = 725)
int hygro_ext;

SHT1x sht1x(11, 12);

void init_hygro()
{
    Serial.println("-- Initialisation: Hygrometrie");
}

void mesure_hygro()
{
    Serial.println("-- Mesure: Hygrometrie");

    float hygroi = sht1x.readHumidity();
    hygro_int = hygroi * 10;
}

void envoie_hygro()
{
    envoie_mqtt("bees/hygro/int", hygro_int);
    envoie_mqtt("bees/hygro/ext", hygro_ext);
}

/* ----------- *
 * TEMPERATURE *
 * ----------- */

// Entree Arduino
#define BROCHE 7

// Commandes
#define PRISE_TEMP 0x44
#define LECTURE_SCRATCHPAD 0xBE

// Adresses des capteurs, chaques adresse est unique a chaques capteurs
const byte ADRESSE_INT[]{0x28, 0xFF, 0xAD, 0x58, 0x60, 0x17, 0x5, 0x4B};
const byte ADRESSE_EXT[]{0x28, 0xFF, 0xC8, 0x71, 0x60, 0x17, 0x3, 0xB6};

float temp_int = .0f, temp_ext = .0f;
OneWire capteur(BROCHE);

float lisTemp(const byte *adresse)
{
    byte donnees[9];

    capteur.reset();
    capteur.select(adresse);

    // Lance la prise de temperature
    capteur.write(PRISE_TEMP, 1);
    delay(250); // Le capteur prends la mesure

    // Lecture des resultats
    capteur.reset();
    capteur.select(adresse);
    capteur.write(LECTURE_SCRATCHPAD);

    bool ok = true;

    // Lecture des donnees du scratchpad
    for (byte i = 0; i < 9; i++)
    {
        donnees[i] = capteur.read();

        if (donnees[i] == 0xff)
            ok = false;
        else
            ok = true;
    }

    // Une erreur pendant la mesure
    if (!ok)
    {
        for (int i = 0; i < 8; i++)
        {
            Serial.print(adresse[i], HEX);
            Serial.print(' ');
        }

        Serial.print("est inaccessible ! ");
        return -999.0f;
    }

    return ((int16_t)((donnees[1] << 8) | donnees[0])) * 0.0625; // Plus petite variations pour une resolution de 12bits
}

void mesure_temp()
{
    Serial.print("Temperature interieur: ");
    temp_int = lisTemp(ADRESSE_INT);
    Serial.print(temp_int);
    Serial.print('\n');

    Serial.print("Temperature exterieur: ");
    temp_ext = lisTemp(ADRESSE_EXT);
    Serial.print(temp_ext);
    Serial.print('\n');
}

void envoie_temp()
{
    envoie_mqtt("bees/temp/int", temp_int);
    envoie_mqtt("bees/temp/ext", temp_ext);
}

/* ----
 * BATTERY
 * ---- */
/*
void init_battery()
{
  int battery_voltage = A0;
  int battery_level;
}

void mesure_battery()
{
  battery_level = map(analogRead(battery_voltage), 3, 4, 15, 100);

  //à enlever pour integrer à electron
  Serial.print("Batterie : ");
  Serial.print(battery_level);

  if (battery_level < 100 && battery_level > 70)
  {
    Serial.println("Batterie pleine");
  }
  
  if (battery_level < 70 && battery_level > 30)
  {
    Serial.println("Batterie à moitié pleine");
  }
  
  if (battery_level < 30 && battery_level > 0)
  {
    Serial.println("Batterie vide");
  }
}

void envoie_battery()
{
  envoie_mqtt("Battery_level", battery_level);
}
*/

/* ---- *
 * WIFI *
 * ---- */

const int WIFI_ENPIN = 5;
const char WIFI_SSID[] = ".";
const char WIFI_PASSWD[] = "coincoin";
const char WIFI_MQTTHOST[] = "192.168.43.41";

WiFiEspClient WIFI_CLIENT;
PubSubClient MQTT_CLIENT(WIFI_CLIENT);

void init_wifi()
{
#ifndef DEBUG
    Serial.println("-- Initialisation: Wifi");
    Serial3.begin(115200);

    pinMode(WIFI_ENPIN, OUTPUT);
    digitalWrite(WIFI_ENPIN, LOW);
    delay(100);
    digitalWrite(WIFI_ENPIN, HIGH);

    WiFi.init(&Serial3);
    if (WiFi.status() == WL_NO_SHIELD)
    {
        Serial.println("ESP NOT FOUND");
        digitalWrite(LED_BUILTIN, HIGH);
        while (true)
            ;
    }
#endif
}

void wifi_on()
{
#ifndef DEBUG
    init_wifi();

    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print("-- Connection a ");
        Serial.print(WIFI_SSID);
        Serial.print(" avec mot de passe: ");
        Serial.println(WIFI_PASSWD);

        WiFi.begin(WIFI_SSID, WIFI_PASSWD);
    }

    Serial.print("-- Connection OK");
    MQTT_CLIENT.setServer(WIFI_MQTTHOST, 1883);

    do
    {
        delay(500);
        Serial.print("-- Connection a ");
        Serial.println(WIFI_MQTTHOST);
    } while (!MQTT_CLIENT.connect("ruche"));

    Serial.println("-- Connection OK");
#endif
}

void wifi_off()
{
#ifndef DEBUG
    MQTT_CLIENT.disconnect();
    WiFi.disconnect();

    digitalWrite(WIFI_ENPIN, LOW);

    Serial.println("-- Wifi eteint");
#endif
}

void envoie_mqtt(const char *topic, long val)
{
    Serial.print("ENVOIE\tCanal: ");
    Serial.print(topic);
    Serial.print("\tMessage: ");
    ltoa(val, MQTT_BUF1, 10);
    Serial.print(MQTT_BUF1);
    Serial.print('\n');

#ifndef DEBUG
    MQTT_CLIENT.publish(topic, MQTT_BUF1, true);
#endif
}

void envoie_tout()
{
    wifi_on();

    envoie_masse();
    envoie_freq();
    envoie_hygro();
    envoie_temp();

    wifi_off();
}

/* ---- *
 * BASE *
 * ---- */

const long CYCLE_TOTAL = 10 * 1000;
const long CYCLE_PHT = 5 * 1000;
const long CYCLE_FREQ = 1000;

void setup()
{
    Serial.begin(115200);

    Serial.println("-- Initialisation");

#ifdef DEBUG
    Serial.println("/!\\ /!\\ DEBUG actif, pas de wifi ni MQTT /!\\ /!\\");
#endif

    pinMode(LED_BUILTIN, OUTPUT);

    for (int i = 0; i < 3; i++)
    {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(200);
        digitalWrite(LED_BUILTIN, LOW);
        delay(200);
    }

    init_wifi();
    init_masse();
    init_freq();
    init_hygro();

    Serial.println("-- Initialisation OK");

    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
}

void loop()
{
    for (int i = 0; i < CYCLE_TOTAL / CYCLE_PHT; i++)
    {
        for (int j = 0; j < CYCLE_PHT / CYCLE_FREQ; j++)
        {
            digitalWrite(LED_BUILTIN, LOW);
            delay(CYCLE_FREQ);
            digitalWrite(LED_BUILTIN, HIGH);

            mesure_freq();
        }

        mesure_masse();
        mesure_hygro();
        mesure_temp();
    }

    envoie_tout();
}