

#include "HX711.h"

#define calibration_factor1 -126870
#define calibration_factor2 -128470 
#define calibration_factor3 -128790 
#define calibration_factor4 -134610 


HX711 scale1(3, 2); //BD
HX711 scale2(4, 2); //BG
HX711 scale3(5, 2); //HG
HX711 scale4(6, 2); //HD

void setup() {
  Serial.begin(9600);
  Serial.println("Ruche - Mesure de masse");

  scale1.set_scale(calibration_factor1); 
  scale1.tare();

  scale2.set_scale(calibration_factor2); 
  scale2.tare(); 

   scale3.set_scale(calibration_factor3); 
  scale3.tare(); 

   scale4.set_scale(calibration_factor4); 
  scale4.tare(); 
  
}

void loop() {

#define Scale1 scale1.get_units(), 3
#define Scale2 scale2.get_units(), 3
#define Scale3 scale3.get_units(), 3
#define Scale4 scale4.get_units(), 3

float Somme;
float Load1;
float Load2;
float Load3;
float Load4;

Load1 = Scale1;
Load2 = Scale2;
Load3 = Scale3;
Load4 = Scale4;
Somme = Load1 + Load2 + Load3 + Load4;

  Serial.println();
  Serial.print("Reading:  ");
  Serial.print("BD ");
  Serial.print(Scale1);
  Serial.print("  ");
    Serial.print("BG ");
  Serial.print(Scale2);
  Serial.print("  ");
    Serial.print("HG ");
  Serial.print(Scale3); 
  Serial.print("  ");
    Serial.print("HD ");
  Serial.print(Scale4);
  Serial.print("  ");
  Serial.print("Somme: ");
  Serial.print(Somme, 3);
  delay(500);

  if(Serial.available())
  {
    char reset = Serial.read();
    if(reset == '0')
      scale1.tare();
      scale2.tare();
      scale3.tare();
      scale4.tare();
      Serial.println(); 
      Serial.println("RESET");    
  }
}
