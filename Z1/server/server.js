const fs = require("fs");
const mqtt = require("mqtt").connect("mqtt://localhost:1883");
const Sockets = require("zsockets");

const WSS = new Sockets.WebSocketServer(8080, () => {
    console.log(":: Server Started, listening on port 8080");
});

/* const cdata = {
    hygro: {i: 0.0, e: 0.0},
    temp: {i: 0.0, e: 0.0},
    freq: {avg: 0.0, arr: []},
    masse: {vald: 0.0, x: 0.0, y: 0.0},
}; */

randint = function(max)
{
    return Math.floor(Math.random() * max);
}

randfloat = function(max)
{
    return (Math.random() * max).toFixed(2)
}

const cdata = {
    hygro: {i: randint(35), e: randint(25)},
    temp: {i: randint(35), e: randint(25)},
    freq: {avg: randint(1500), arr: []},
    masse: {vald: randint(35), x: randfloat(1), y: randfloat(1)},
};

class DataStorer {
    constructor()
    {
        this.data = [];

        fs.readFile("./data.json", (err, gdata) => {
            if (err)
            {
                if (err.errno === -2)
                {
                    fs.writeFile("./data.json", JSON.stringify(this.data), () => {});
                }
            }
            else
            {
                try
                {
                    this.data = JSON.parse(gdata);
                }
                catch (err)
                {
                    console.log(err)
                }
            }
        });

        setInterval(() => {
            this.updateDaily();
        }, 60000);
    }

    updateDaily()
    {
        if (this.data.length == 29)
            this.data.splice(0, 1);

        this.data[this.data.length] = cdata;

        fs.writeFile("./data.json", JSON.stringify(this.data), () => {
            console.log(":: Daily data saved");
        });
    }
};

process.argv.forEach((val, index, array) => {
    switch (val)
    {
        case "-test":
            console.log("RUNNING IN TEST MODE");
            setTimeout(() => {
                process.exit(0);
            }, 2500);
            break;
    }
});

const dataStorer = new DataStorer;

mqtt.on("connect", (client) => {
    mqtt.subscribe("bees/#", (err) => {});

    console.log("-> Connected to broker");

    mqtt.on("message", (topic, message) => {
        const msg = JSON.parse(message);

        switch (topic)
        {
            case "bees/hygro/int":
                console.log("%s : %d", "Humidité I", msg);
                cdata.hygro.i = msg;

                break;
            case "bees/hygro/ext":
                console.log("%s : %d", "Humidité E", msg);
                cdata.hygro.e = msg;

                break;
            case "bees/temp/int":
                console.log("%s : %d", "Temperature", msg);
                cdata.temp.i = msg;

                break;
            case "bees/temp/ext":
                console.log("%s : %d", "Temperature", msg);
                cdata.temp.e = msg;

                break;
            case "bees/count/avg":
                console.log("%s : %d", "Nbr d'abeilles", msg);
                cdata.freq.avg = msg;

                break;
            case "bees/count/arr":
                console.log("%s : %d", "Nbr d'abeilles", msg);
                cdata.freq.arr = msg;

                break;
            case "bees/weight/vald":
                console.log("%s : %d", "Poid", msg);
                cdata.masse.vald = msg;

                break;
            case "bees/weight/pos":
                console.log("%s : %d", "Poid", msg);
                cdata.masse.x = msg[0];
                cdata.masse.y = msg[1];

                break;

            default:
                //
        }

        console.log("-> Data received from broker");

        WSS.EmitToAll("updatedata", cdata);
    });
});

WSS.OnInternal("connection", (c) => {
    console.log("Client connected");

    c.Emit("firstdata", cdata);

    setTimeout(() => {
        if (dataStorer.data.length > 0)
        {
            c.Emit("dailydata", dataStorer.data);
        }
    }, 250);
});